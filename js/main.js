let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

function renderGlassesList() {
  let rowContent = `<div class="row justify-content-between">`;
  for (let glassesIndex in dataGlasses) {
    rowContent += `<div class="col-md-4 align-self-center" 
    onclick = "wearGlasses('${dataGlasses[glassesIndex].id}')">
        <img src="${dataGlasses[glassesIndex].src}" class="img-fluid px-3" />
    </div>`;
    if ((glassesIndex + 1) % 3 == 0 && glassesIndex + 1 < dataGlasses.length) {
      rowContent += `</div><div class="row justify-content-between"> `;
    }
  }
  rowContent += "</div>";
  document.getElementById("vglassesList").innerHTML = rowContent;
}

renderGlassesList();

function wearGlasses(id) {
  let item = dataGlasses.find((item) => item.id == id);
  if (item != null) {
    let rowContent = `<p><span id="" class="h6 mr-3">${item.name}</span><span>(${item.color})</span><span id="glass_id">${item.id}</span></p>
    <p><span class="bg-danger p-2 mr-3">$${item.price}</span><span class="text-primary">${item.brand}</span></p>
    <p>${item.description}</p>`;

    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("glassesInfo").innerHTML = rowContent;
  }
}

function removeGlasses(booleanvalue) {
  const divEl = document.getElementById("glassesInfo");
  const display = window.getComputedStyle(divEl).display;
  if (booleanvalue == true && display == "block") {
    let id = document.getElementById("glass_id").textContent;
    let index = dataGlasses.findIndex((item) => {
      return item.id == id;
    });
    let rowContent = `<img src="${dataGlasses[index].virtualImg}" class="img-fluid alt=""/>`;
    document.getElementById("avatar").innerHTML = rowContent;
  } else {
    document.getElementById("avatar").innerHTML = "";
  }
}
