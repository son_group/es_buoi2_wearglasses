export class Glasses{
    constructor(id,src,virtualImage,brand,name,color,price,description){
        this.id = id;
        this.src = src;
        this.virtualImage = virtualImage;
        this.brand = brand;
        this.name = name;
        this.color = color;
        this.price = price;
        this.description = description;        
    }
}